﻿#include "Ball.h"

Ball::Ball(const float x, const float y, const float speedX, const float speedY, const float radius)
	: Object(x, y), SpeedX(speedX), SpeedY(speedY), Radius(radius)
{
}

Ball::Ball(): Object(0,0), SpeedX(0), SpeedY(0), Radius(0)
{
}

void Ball::Draw() const
{
	RAYLIB_H::DrawCircle((int)X, (int)Y, Radius, RAYLIB_H::WHITE);
}
