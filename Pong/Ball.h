﻿#pragma once
#include "Object.h"
#include "raylib.h"

struct Ball final : public Object
{
	float SpeedX, SpeedY, Radius;

	Ball(float x, float y, float speedX, float speedY, float radius);
	Ball();

	void Draw() const override;
};
