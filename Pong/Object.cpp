﻿#include "Object.h"

Object::Object(const float x, const float y): X(x), Y(y)
{
}

Object::~Object() = default;

void Object::Draw() const
{
}
