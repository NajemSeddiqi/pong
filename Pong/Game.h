#pragma once
#include <functional>
#include "Ball.h"
#include "Paddle.h"

class Game
{
private:
	const char* name = "Pong";
	bool initialized = false;
	Ball ball;
	Paddle leftPaddle;
	Paddle rightPaddle;
	const char* winnerText = nullptr;
	static void SetXAndYBallSpeed(Ball& ball);
	static void SetVerticalBordersFor(Object& obj);
	static void IsKeyDown(int key, const std::function<void()>& action);
	static void IfCollisionCircleRec(Object& circle, Object& rec, const std::function<void(Object& circleObj, Object& recObj)>& action);
	void WhoWins(const Ball& ballObj);
	void IfReset(Ball& ballObj);
	void DisplayCenteredWinnerText() const;
	template<typename... Args>
	static void DrawObjects(Args... args);

public:
	Game(int windowWidth, int windowHeight);
	Game(const Game& other) = delete;
	Game& operator=(const Game& other) = delete;
	Game(const Game&& other) = delete;
	Game& operator=(const Game&& other) = delete;
	~Game() noexcept;

	void Start();
};
