#include "Game.h"
#include <iostream>
#include "Ball.h"
#include "Paddle.h"

void Game::SetXAndYBallSpeed(Ball& ball)
{
	ball.X += ball.SpeedX * RAYLIB_H::GetFrameTime();
	ball.Y += ball.SpeedY * RAYLIB_H::GetFrameTime();
}

void Game::SetVerticalBordersFor(Object& obj)
{
	if (obj.Y < 0)
	{
		obj.Y = 0;
		if (dynamic_cast<Ball*>(&obj))
		{
			auto& ballRef = dynamic_cast<Ball&>(obj);
			ballRef.SpeedY *= -1;
		}
	}

	if (obj.Y > (float)RAYLIB_H::GetScreenHeight())
	{
		obj.Y = (float)RAYLIB_H::GetScreenHeight();
		if (dynamic_cast<Ball*>(&obj))
		{
			auto& ballRef = dynamic_cast<Ball&>(obj);
			ballRef.SpeedY *= -1;
		}
	}
}

void Game::IsKeyDown(const int key, const std::function<void()>& action)
{
	if (RAYLIB_H::IsKeyDown(key)) action();
}

void Game::IfCollisionCircleRec(Object& circle, Object& rec, const std::function<void(Object& circleObj, Object& recObj)>& action)
{
	if (CheckCollisionCircleRec(RAYLIB_H::Vector2{ circle.X, circle.Y }, dynamic_cast<Ball&>(circle).Radius, dynamic_cast<Paddle&>(rec).GetRect()))
		action(circle, rec);
}

void Game::WhoWins(const Ball& ballObj)
{
	if (ballObj.X < 0) winnerText = "Right player wins!";

	if ((int)ballObj.X > RAYLIB_H::GetScreenWidth()) winnerText = "Left player wins!";
}

void Game::IfReset(Ball& ballObj)
{
	if (winnerText && RAYLIB_H::IsKeyPressed(KEY_SPACE))
	{
		ballObj.X = (float)RAYLIB_H::GetScreenWidth() / 2;
		ballObj.Y = (float)RAYLIB_H::GetScreenHeight() / 2;
		ballObj.SpeedX = 300;
		ballObj.SpeedY = 300;
		winnerText = nullptr;
	}
}

void Game::DisplayCenteredWinnerText() const
{
	if (winnerText)
	{
		const int textWidth = RAYLIB_H::MeasureText(winnerText, 60) - 230;
		RAYLIB_H::DrawText(winnerText, RAYLIB_H::GetScreenWidth() / 2 - textWidth, RAYLIB_H::GetScreenHeight() / 2 - 30, 60, RAYLIB_H::YELLOW);
	}
}

//For mixed variadic types, use this kind of fold expression (c++17) with a delegate/callable
template <typename ... Args>
void Game::DrawObjects(Args... args)
{
	([&](auto& arg) {static_cast<Object&>(arg).Draw(); } (args), ...);

	//Longer but more legible version of above
	/*template <typename TDelegate, typename T, typename ... Args>
	void DoFor(TDelegate delegate, T first, Args... args)
	{
		delegate(first);
		DoFor(delegate, args...);
	}

	template<typename... Args>
	void DrawObjects(Args... args)
	{
		DoFor([&](auto arg)
			{
				static_cast<Object&>(arg).Draw();
			}, args...);
	}*/
}

Game::Game(const int windowWidth, const int windowHeight)
{
	if (initialized) return;

	RAYLIB_H::InitWindow(windowWidth, windowHeight, name);
	RAYLIB_H::SetWindowState(FLAG_VSYNC_HINT);
	initialized = true;

	ball = Ball((float)GetScreenWidth() / 2.0f, (float)GetScreenHeight() / 2.0f, 300, 300, 5);
	leftPaddle = Paddle(50, (float)GetScreenHeight() / 2, 500, 10, 100);
	rightPaddle = Paddle((float)GetScreenWidth() - 50, (float)GetScreenHeight() / 2, 500, 10, 100);
}

void Game::Start()
{
	winnerText = nullptr;

	while (!WindowShouldClose())
	{
		SetXAndYBallSpeed(ball);

		SetVerticalBordersFor(ball);
		SetVerticalBordersFor(leftPaddle);
		SetVerticalBordersFor(rightPaddle);

		IsKeyDown(KEY_W, [this] { leftPaddle.Y -= leftPaddle.Speed * RAYLIB_H::GetFrameTime(); });
		IsKeyDown(KEY_S, [this] { leftPaddle.Y += leftPaddle.Speed * RAYLIB_H::GetFrameTime(); });
		IsKeyDown(KEY_UP, [this] { rightPaddle.Y -= rightPaddle.Speed * RAYLIB_H::GetFrameTime(); });
		IsKeyDown(KEY_DOWN, [this] { rightPaddle.Y += rightPaddle.Speed * RAYLIB_H::GetFrameTime(); });

		auto onCollisionCircleRec = [](Ball& ballObj, const Paddle& paddle)
		{
			ballObj.SpeedX *= -1.1f;
			ballObj.SpeedY = (ballObj.Y - paddle.Y) / (paddle.Height / 2) * -ballObj.SpeedX;
		};

		IfCollisionCircleRec(ball, leftPaddle, [onCollisionCircleRec](Object& circleObj, Object& recObj)
			{
				if (auto& ballRef = dynamic_cast<Ball&>(circleObj); ballRef.SpeedX < 0)
					onCollisionCircleRec(ballRef, dynamic_cast<Paddle&>(recObj));
			});

		IfCollisionCircleRec(ball, rightPaddle, [onCollisionCircleRec](Object& circleObj, Object& recObj)
			{
				if (auto& ballRef = dynamic_cast<Ball&>(circleObj); ballRef.SpeedX > 0)
					onCollisionCircleRec(ballRef, dynamic_cast<Paddle&>(recObj));
			});

		WhoWins(ball);
		IfReset(ball);

		RAYLIB_H::BeginDrawing();
		RAYLIB_H::ClearBackground(RAYLIB_H::BLACK);

		DrawObjects(ball, leftPaddle, rightPaddle);

		DisplayCenteredWinnerText();

		RAYLIB_H::DrawFPS(10, 10);
		RAYLIB_H::EndDrawing();
	}
}

Game::~Game() noexcept
{
	RAYLIB_H::CloseWindow();
}
