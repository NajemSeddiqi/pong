﻿#pragma once
#include "raylib.h"

struct Object
{
	float X, Y;
	Object(float x, float y);
	virtual ~Object();
	virtual void Draw() const;
};
