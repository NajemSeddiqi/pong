﻿#pragma once
#include <raylib.h>

#include "Object.h"

struct Paddle final : public Object
{
	float Speed, Width, Height;

	Paddle(const float x, const float y, const float speed, const float width, const float height);
	Paddle();

	[[nodiscard]] Rectangle GetRect() const;

	void Draw() const override;
};
