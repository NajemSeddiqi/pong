﻿#include "Paddle.h"


Paddle::Paddle(const float x, const float y, const float speed, const float width, const float height) :
	Object(x,y), Speed(speed), Width(width), Height(height)
{
}

Paddle::Paddle(): Object(0,0), Speed(0), Width(0), Height(0)
{
}

Rectangle Paddle::GetRect() const
{
	return RAYLIB_H::Rectangle{ X - Width / 2, Y - Height / 2, Width, Height };
}

void Paddle::Draw() const
{
	RAYLIB_H::DrawRectangleRec(GetRect(), RAYLIB_H::WHITE);
}